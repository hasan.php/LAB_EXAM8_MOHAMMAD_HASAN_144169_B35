<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;

if(!isset( $_SESSION)) session_start();
echo "<p id='message'>".Message::getMessage()."</p>";

?>

<!DOCTYPE html>
<html lang="en">
<title>ProfilePicture</title>
<head>


    <!-- CSS -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="../../../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../assets/css/form-elements.css">
    <link rel="stylesheet" href="../../../assets/css/style.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="../https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="../https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Favicon and touch icons -->
    <link rel="shortcut icon" href="../../../assets/ico/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../../../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../../../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposedaa" sizes="72x72" href="../../../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../../../assets/ico/apple-touch-icon-57-precomposed.png">

</head>

<body>

<!-- Top content -->
<div class="top-content">


    <div class="container">


        <div class="col-sm-6 col-sm-offset-3 form-box">
            <div class="form-top">
                <div class="form-top-left">
                    <h3>Profile Picture</h3>
                    <p>Enter Name And Profile Picture</p>
                </div>
                <div class="form-top-right">
                    <i class="fa fa-book"></i>
                </div>
            </div>
            <div class="form-bottom">
                <form role="form" action="store.php" method="post" class="login-form" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="sr-only" for="form-book_title">Name</label>
                        <input type="text" name="name" placeholder="Name...." class="form-username form-control" id="form-username">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="form-author_name">Image</label>
                        Image:
                        <input type="file" name="image" placeholder="choose image" value="choose image">

                    </div>
                    
                    <button type="submit" class="btn">Submit</button>
                </form>
            </div>
        </div>


    </div>
</div>

</div>


<!-- Javascript -->
<script src="../../../assets/js/jquery-1.11.1.min.js"></script>
<script src="../../../assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../../../assets/js/jquery.backstretch.min.js"></script>
<script src="../../../assets/js/scripts.js"></script>

<!--script for message fadeout-->
<script>
    jQuery(document).ready(function($){
        $('#message').fadeOut(5555);


    });

</script>

</body>

</html>