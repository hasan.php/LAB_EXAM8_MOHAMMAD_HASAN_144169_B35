<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;

if(!isset( $_SESSION)) session_start();
echo Message::message();
use App\Hobbies\Hobbies;
$bookinfo=new Hobbies();
$bookinfo->setData($_GET);
$oneData=$bookinfo->view("obj");






?>

<!DOCTYPE html>
<html lang="en">
<title>Edit-BookTitle</title>
<head>


    <!-- CSS -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="../../../assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../assets/css/form-elements.css">
    <link rel="stylesheet" href="../../../assets/css/style.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="../https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="../https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Favicon and touch icons -->
    <link rel="shortcut icon" href="../../../assets/ico/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../../../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../../../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../../../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../../../assets/ico/apple-touch-icon-57-precomposed.png">

</head>

<body>

<!-- Top content -->
<div class="top-content">


    <div class="container">

        <div class="row">
            <div class="col-sm-6 col-sm-offset-3 form-box">
                <div class="form-top">
                    <div class="form-top-left">
                        <h3>Hobbies-Edit</h3>

                        <p>Edit Hobbies and choose new</p>
                    </div>
                    <div class="form-top-right">
                        <i class="fa-book"></i>
                    </div>
                </div>
                <div class="form-bottom">
                    <form role="form" action="update.php" method="post" class="login-form">
                        <input type="hidden" name="id" value="<?php echo $oneData->id?>">

                        <div class="form-group">
                            <label class="sr-only" for="form-name">Name</label>

                            <input type="text" name="name" value="<?php echo $oneData->name?>" class="form-password form-control" id="form-password">
                        </div>
                        <div >
                            <table >
                                <tr>
                                    <td colspan="2">Select Hobbies:</td>
                                </tr>
                                <tr>
                                    <td>Walking</td>
                                    <td><input type="checkbox" name="hobbies[]" value="cricket" <?php if (in_array("cricket",explode(',',$oneData->hobbies))) {echo "checked";}?> </td>
                                </tr>
                                <tr>
                                    <td>Swimming</td>
                                    <td><input type="checkbox" name="hobbies[]" value="swimming" <?php if (in_array("swimming",explode(',',$oneData->hobbies))) {echo "checked";}?>></td>
                                </tr>
                                <tr>
                                    <td>Gardening</td>
                                    <td><input type="checkbox" name="hobbies[]" value="gardening" <?php if (in_array("gardening",explode(',',$oneData->hobbies))) {echo "checked";}?>></td>
                                </tr>
                                <tr>
                                    <td>Watching TV</td>
                                    <td><input type="checkbox" name="hobbies[]" value="fishing" <?php if (in_array("fishing",explode(',',$oneData->hobbies))) {echo "checked";}?>></td>
                                </tr>
                                <tr>
                                    <td>Others</td>
                                    <td><input type="checkbox" name="hobbies[]" value="others" <?php if (in_array("others",explode(',',$oneData->hobbies))) {echo "checked";}?>></td>
                                </tr>

                            </table>
                        </div>
                        <button type="submit" class="btn">Update</button>

                    </form>
                </div>
            </div>
        </div>



        <!-- Javascript -->
        <script src="../../../assets/js/jquery-1.11.1.min.js"></script>
        <script src="../../../assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="../../../assets/js/jquery.backstretch.min.js"></script>
        <script src="../../../assets/js/scripts.js"></script>

        <!--[if lt IE 10]>
        <script src="../../../assets/js/placeholder.js"></script>
        <![endif]-->

</body>

</html>