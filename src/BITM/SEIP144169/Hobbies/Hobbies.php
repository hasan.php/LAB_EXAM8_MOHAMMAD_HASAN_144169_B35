<?php
namespace App\Hobbies;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

use PDO;


class Hobbies extends DB
{
    public $id;
    public $name;
    public $hobbies;

    public function __construct()
    {
        parent::__construct();
    }



    public function setData($postVariableData = NULL)
    {

        if (array_key_exists('id', $postVariableData)) {
            $this->id = $postVariableData['id'];
        }
        if (array_key_exists('name', $postVariableData)) {
            $this->name = $postVariableData['name'];

        }
        if (array_key_exists('hobbies', $postVariableData)) {
            $this->hobbies = $postVariableData['hobbies'];
        }

    }//end of setData method

    public function store()
    {    $str = implode(',', $this->hobbies);
        $arrData = array($this->name,$str);
        $sql = "Insert INTO hobbies(name,hobbies) VALUES (?,?)";
        $STH = $this->DBH->prepare($sql);
        $result = $STH->execute($arrData);
        if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted Successfully :(");


        Utility::redirect('create.php');

    }

    public function index($fetchMode='ASSOC'){

        $STH = $this->DBH->query("SELECT * from hobbies where is_deleted='No'");

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();




    public function view($fetchMode='ASSOC'){

        $STH = $this->DBH->query('SELECT * from hobbies where id='.$this->id);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;


    }// end of view();

    public function update(){
        $arrData=array($this->name,$this->hobbies);

        $sql="UPDATE hobbies SET name = ?, hobbies = ? WHERE id=".$this->id;
        /// echo $sql;die();
        $STH=$this->DBH->prepare($sql);
        $result=$STH->execute($arrData);

        Utility::redirect('index.php');



    }//end of update method



    public function delete(){


        //var_dump($this->id);die();

        $sql="Delete from hobbies WHERE id=".$this->id;

        $STH=$this->DBH->prepare($sql);
        $STH->execute();

        Utility::redirect($_SERVER['HTTP_REFERER']);




    }
    public function trash(){


        //var_dump($this->id);die();

        $sql="Update hobbies SET is_deleted=NOW() WHERE id=".$this->id;

        $STH=$this->DBH->prepare($sql);
        $STH->execute();

        Utility::redirect('index.php');




    }

    public function trashed($fetchMode='ASSOC'){

        $STH = $this->DBH->query("SELECT * from hobbies where is_deleted<>'No'");

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of trashed();


    public function recover(){

        $sql = "Update hobbies SET is_deleted='No' where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('trashed.php');

    }// end of recover();







}
