<?php
namespace App\ProfilePicture;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;



class ProfilePicture extends DB
{
    public $id;
    public $name;
    public $image;


    public function __construct()
    {

        parent::__construct();
    }


    public function setData($data = NULL)
    {
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('name', $data)) {
            $this->name = $data['name'];
        }

        if (array_key_exists('image', $data)) {
            $this->image = $data['image']['name'];
        }

    }

    public function store()
    {
        $path='C:\xampp\htdocs\LabExam8_Mohammad_Hasan_144169_b35\Upload/';
        $uploadedFile = $path.basename($this->image);
        move_uploaded_file($_FILES['image']['tmp_name'], $uploadedFile);


        $arrData = array($this->name, $this->image);

        $sql = "insert into profile_picture(name, image) VALUES (?,?)";

        $STH = $this->DBH->prepare($sql); //create a object
        $result = $STH->execute($arrData);
        if ($result)
            Message::setMessage("Success! Data has been inserted successfully!");
        else
            Message::setMessage("Fail! Data has not been inserted successfully");

        Utility::redirect('create.php');

    }


    public function index($fetchMode='ASSOC'){
        $sql= "SELECT * from profile_picture where is_deleted='No'";


        $STH = $this->DBH->query($sql);
        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();


    public function view($fetchMode='ASSOC'){

        $sql='SELECT * from profile_picture WHERE id='.$this->id;

        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;


    }// end of view();

    public function update(){
        $path='C:\xampp\htdocs\LabExam8_Mohammad_Hasan_144169_b35\upload/';
        if(!empty($_FILES['image']['name'])) {
            $uploadedFile = $path.basename($this->image);
            move_uploaded_file($_FILES['image']['tmp_name'],$uploadedFile);
            $arrData = array( $this->name,$this->image);
            $sql="UPDATE profile_picture SET name = ?, image = ? WHERE id=".$this->id;}
        else{
            $arrData = array( $this->name);
            $sql="UPDATE profile_picture SET name = ? WHERE id=".$this->id;
        }

        $STH=$this->DBH->prepare($sql);
        $STH->execute($arrData);
        Utility::redirect('index.php');

    }// end of update method;
    public function delete(){
        $sql= "DELETE FROM profile_picture WHERE id =". $this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect('index.php');

    }
    public function trash(){


        $sql= "UPDATE profile_picture SET is_deleted= NOW() WHERE id =". $this->id;


        $STH=$this->DBH->prepare($sql);
        $STH->execute();

        Utility::redirect('index.php');


    }







}
