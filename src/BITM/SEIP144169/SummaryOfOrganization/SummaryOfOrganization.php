<?php
namespace App\SummaryOfOrganization;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;


class SummaryOfOrganization extends DB
{
    public $id;
    public $name;
    public $summary;

    public function __construct(){

        parent::__construct();
    }

    public function setData($data=NULL){
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('name',$data)){
            $this->name=$data['name'];
        }
        if(array_key_exists('summary',$data)){
            $this->summary=$data['summary'];
        }
    }
    public function store(){
        $arrData = array($this->name,$this->summary);
        $sql="insert into summary_of_organization(name, summary) VALUES (?,?)";

        $STH= $this->DBH->prepare($sql); //create a object
        $result= $STH->execute($arrData);
        if($result)
            Message::setMessage("Success! Data has been inserted successfully!");
        else
            Message::setMessage("Fail! Data has not been inserted successfully");

        Utility::redirect('create.php');

    }


    public function index($fetchMode='ASSOC'){
        $sql= "SELECT * from summary_of_organization where is_deleted='No'";


        $STH = $this->DBH->query($sql);
        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of index();


    public function view($fetchMode='ASSOC'){

        $sql='SELECT * from summary_of_organization WHERE id='.$this->id;

        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;


    }// end of view();

    public function update(){
        $arrData = array( $this->name,$this->summary);

        $sql="UPDATE summary_of_organization SET name = ?, summary = ? WHERE id=".$this->id;

        $STH=$this->DBH->prepare($sql);

        $STH->execute($arrData);

        Utility::redirect('index.php');






    }// end of update method;
    public function delete(){
        $sql= "DELETE FROM summary_of_organization WHERE id =". $this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect('index.php');

    }
    public function trash(){

        $sql= "UPDATE summary_of_organization SET is_deleted=NOW() WHERE id =". $this->id;


        $STH=$this->DBH->prepare($sql);
        $STH->execute();

        Utility::redirect('index.php');


    }











}

